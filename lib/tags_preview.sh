#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset

tag="${1}"

zk-fts --tags "${tag#\#}"
