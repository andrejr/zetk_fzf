#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset

script_dir=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd -P)
lib_dir="$script_dir"

source "$script_dir/common.sh"

file="$1"

document_picker "zk-sim --tags --print0 \"$file\"" \
	"Documents with tags similar to '$1'"
