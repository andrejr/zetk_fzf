#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset

op="$1"
target="$2"

case "$op" in
	t)
        no_tag_target="${target#\#}"
		tmux send-keys -t '{top-left}' Escape :read Space ! Space echo Space \
			&& tmux send-keys -t '{top-left}' -l \'"\#${no_tag_target}"\' \
			&& tmux send-keys -t '{top-left}' Enter
		;;
	i)
		tmux send-keys -t '{top-left}' Escape :read Space ! Space echo Space \
			&& tmux send-keys -t '{top-left}' -l \'"[[${target}]]"\' \
			&& tmux send-keys -t '{top-left}' Enter
		;;
	l)
		tmux send-keys -t '{top-left}' Escape :read Space ! Space echo Space \
			&& tmux send-keys -t '{top-left}' -l \'"[${target}](${target})"\' \
			&& tmux send-keys -t '{top-left}' Enter
		;;
	e)
		tmux send-keys -t '{top-left}' :e Space \
			&& tmux send-keys -t '{top-left}' -l "${ZK_PATH}/${target}" \
			&& tmux send-keys -t '{top-left}' Enter
		;;
	sp)
		tmux send-keys -t '{top-left}' :sp Space \
			&& tmux send-keys -t '{top-left}' -l "${ZK_PATH}/${target}" \
			&& tmux send-keys -t '{top-left}' Enter
		;;
	vsp)
		tmux send-keys -t '{top-left}' :vsp Space \
			&& tmux send-keys -t '{top-left}' -l "${ZK_PATH}/${target}" \
			&& tmux send-keys -t '{top-left}' Enter
		;;
	tab)
		tmux send-keys -t '{top-left}' :tabnew Space \
			&& tmux send-keys -t '{top-left}' -l "${ZK_PATH}/${target}" \
			&& tmux send-keys -t '{top-left}' Enter
		;;
	*)
		echo unknown operation
		;;
esac
