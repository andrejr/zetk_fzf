#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd -P)
lib_dir="$script_dir"
script_path="${BASH_SOURCE[0]}"

# nl=$'\n'
hl=$'\033[93m'
nohl=$'\033[39m'

choices=$(
	cat - << EOF
${hl}lnk${nohl} files linked to in file
${hl}blnk${nohl} files back-linking to file
${hl}sim${nohl} similar files (similar text)
${hl}simt${nohl} similar files (similar tags)
${hl}gv${nohl} neighborhood graph (graphical, links only)
${hl}gvt${nohl} neighborhood graph (graphical, links and tags)
${hl}3${nohl} neighborhood tree selector
${hl}3t${nohl} neighborhood tree selector, common tag connections
${hl}tag${nohl} all tags in file
EOF
)

file="$1"

if [[ -z ${2:-} ]]; then
	echo "$choices" \
		| fzf \
			--ansi \
			--nth=1 \
			--no-multi \
			--cycle \
			--bind "enter:execute[$script_path \"$file\" {}]"
else
	choice="$(echo "$2" | awk '{ print $1; }')"
	case $choice in
		lnk)
			"$lib_dir/links.sh" "$1"
			;;
		blnk)
			"$lib_dir/backlinks.sh" "$1"
			;;
		sim)
			"$lib_dir/sim_txt.sh" "$1"
			;;
		simt)
			"$lib_dir/sim_tags.sh" "$1"
			;;
		gv)
			"$lib_dir/../zkg" "$1"
			;;
		gvt)
			"$lib_dir/../zkg" -t "$1"
			;;
		3)
			"$lib_dir/tree.sh" "$1"
			;;
		3t)
			"$lib_dir/tree.sh" "$1" -t
			;;
		tag)
			"$lib_dir/tif.sh" "$1"
			;;
		*)
			echo unknown command
			echo "$2"
			false
			;;
	esac
	echo "$file"

fi
