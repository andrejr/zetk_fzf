#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset

script_dir=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd -P)
lib_dir="$script_dir"

source "$script_dir/common.sh"

tag="$1"

document_picker "zk-fts --tags \"$tag\"" \
	"Files containing tag '$tag'"
