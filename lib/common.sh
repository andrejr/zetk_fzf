#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset

nl=$'\n'
thl=$'\033[35m'
nohl=$'\033[39m'

function document_picker() {
	local initial_command="$1"
	local header="$2"
	# local on_reload="$3"
	# local help_string="$4"
	# local alt_cmd="$5"

	eval "$initial_command" \
		| SHELL=bash fzf --ansi \
			--read0 \
			--header "${thl}${header}${nohl}$nl<M-c> for context menu${4:+"$4"}" \
			--height 100% \
			--preview-window=top:65%,border-bottom \
			--preview "$lib_dir/fts_preview_wrapper.sh {} {q}" \
			--bind "ctrl-o:execute-silent($lib_dir/tmux.sh i {})" \
			--bind "ctrl-l:execute-silent($lib_dir/tmux.sh l {})" \
			--bind "enter:execute-silent($lib_dir/tmux.sh e {})" \
			--bind "ctrl-v:execute-silent($lib_dir/tmux.sh vsp {})" \
			--bind "ctrl-s:execute-silent($lib_dir/tmux.sh sp {})" \
			--bind "ctrl-t:execute-silent($lib_dir/tmux.sh tab {})" \
			--bind "alt-c:execute($lib_dir/file_ctx_menu.sh {})" \
			${3:+"--bind=change:reload:$3 {q}"} \
			${3:+"--phony"} \
			${5:+"--bind=alt-f:execute($5)"} \
			--no-multi \
			--info=inline
}

function tag_picker() {
	local initial_command="$1"
	local header="$2"
	# local help_string="$3"
	# local alt_cmd="$5"

	eval "$initial_command" \
		| SHELL=bash fzf --ansi \
			--read0 \
			--header "${thl}${header}${nohl}$nl<M-c> for context menu${3:+"$3"}" \
			--height 100% \
			--preview-window=top:65%,border-bottom \
			--preview "$lib_dir/tags_preview.sh {}" \
			--bind "ctrl-o:execute-silent($lib_dir/tmux.sh t {})" \
			--bind "ctrl-l:execute-silent($lib_dir/tmux.sh t {})" \
			--bind "alt-c:execute($lib_dir/tag_ctx_menu.sh {})" \
			--bind "enter:execute($lib_dir/files_containing_tag.sh {})" \
			${4:+"--bind=alt-f:execute($4)"} \
			--no-multi \
			--info=inline
}

function tree_picker() {
	local initial_command="$1"
	local header="$2"

	eval "$initial_command" \
		| SHELL=bash fzf --ansi \
			--header "${thl}${header}${nohl}$nl<M-c> for context menu" \
			--height 100% \
			--preview-window=top:65%,border-bottom \
			--preview "$lib_dir/tree_preview.sh {} {q}" \
			--bind "ctrl-o:execute-silent($lib_dir/tree_tmux.sh i {})" \
			--bind "ctrl-l:execute-silent($lib_dir/tree_tmux.sh l {})" \
			--bind "ctrl-v:execute-silent($lib_dir/tree_tmux.sh vsp {})" \
			--bind "ctrl-s:execute-silent($lib_dir/tree_tmux.sh sp {})" \
			--bind "ctrl-t:execute-silent($lib_dir/tree_tmux.sh tab {})" \
			--bind "alt-c:execute($lib_dir/tree_ctx_menu.sh {})" \
			--bind "enter:execute($lib_dir/tree_select.sh {})" \
			--no-multi \
			--info=inline
}

function is_file() {
	[[ ${1:0:1} != \# ]]
}

function lstrip() {
	echo "${1:-' '}" | sed 's/^[[:space:]]*//g'
}
